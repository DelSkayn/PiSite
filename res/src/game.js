var GameState = {
    DISCONNECTED: 0,
    LOGIN: 1,
    NOT_SPAWNED: 2,
    SPAWNED: 3,
}

var times_helped = 0;

var muted_persons = [];

var Game = function(textbox){
    this.text = textbox;
    this.ws = new WebSocket(addr);
    this.state = GameState.DISCONNECTED;
    this.login_send = false;
    this.name = "";
    this.ws.onmessage = function(event){
        console.log("message: " + event.data);
        handleMessage(event);
    }

    this.ws.onopen = function(event){
        println("Connection gained");
        delayFunction(function(){
            println("Please enter an alias: ");
            game.state = GameState.LOGIN;
        });
    }
    
    this.ws.onclose = function(event){
        println("Disconnected from server!");
        game.state = GameState.DISCONNECTED;
    }
    setTimeout(function(){print()},100);
}

function print(){
    if(!print_que.length == 0){
        $('#textbox').append(print_que.pop());
    }
    setTimeout(function(){print()},100);
}

var print_que = []; 
function println(input){
    print_que.unshift(">" + input + "<br>");
}

var game;

$(document).ready(function(){
    println("Welkom agent: l337.");
    delayFunction(function(){
        println("Connecting to h4x0r-net");
        delayFunction(function(){start()});
    });
});

function anyMessage(msg){
    switch(msg[0]){
        case "new_player":
            println("Player logged in: " + msg[1]);
            return true;
        case "say":
            if(-1 === muted_persons.indexOf(msg[1])){
                println(msg[1] + ":" + msg[2]);
            }
            return true;
        default:
            return false;
    }
}

function anyCommand(command){
    switch(command[0]){
        case "state":
            switch (game.state){
                case GameState.NOT_SPAWNED:
                    println("Your not spawned.");
                    break;
                case GameState.SPAWNED:
                    println("Your are spawned.");
                    break;
            }
            break;
        case "say":
            game.ws.send("say#" + command.slice(1).join(' '));
            return true;
        case "help":
            if(times_helped === 0){
                println("Oh you dont know how to work with h4x0r-net.");
                println("You must be a new recruit.");
                println("Well Let me explain.");
            }else if(times_helped === 1){
                println("You still dont get it?");
                println("Okay.. i will repeat");
            }else if(times_helped === 2){
                println("By know you should know what to do");
                println("Fine! one more time.");
            }else{
                println("nope i am not gonna explain it again.");
                println(" forget it.");
                return true;
            }
            times_helped++;
            println("This is here is the h4x0r-net console.");
            println("it allows you to do your leet work in the dark-net.");
            println("Unfortunatly you are not the only agent here,");
            println("many others are trying to get acces to the dark cornes of the web.");
            println("it is your task to banish them from the net to keep it clean." );
            println("for easy use the commands are based on FPS concepts." );
            println("When your ready to do some work spawn your self in the network.");
            println("If you want to know the commands enter \"comm\" for a handy list.");
            return true;
        case "comm":
            println("#general");
            println(" - say: broadcast a message to other players.");
            println(" - help: get an introductory message.");
            println(" - disable: general function to disable parts of the console.");
            println("       Needs a second command to work for instance: \"disable chat\".");
            println("       #subcommands");
            println("        - chat: disables all communication with other players");
            println(" - mute: disable chats from a certain person needs a name to work.");
            println("       example: \"mute XX_noscopes_XX\".");
            println("#lobby commands");
            println(" - spawn: Spawn you avatar into the darknet and ");
            return true;
        case "mute":
            muted_persons.push(command[1]);
            println("Muted "+ command[1]);
            return true;
        default:
            return false;
    }
}

function loginCommand(msg){
    game.login_send = true;
    game.name = msg;
    game.ws.send("username#"+msg[0]);
}

function loginMessage(msg){
    if(msg[0] === "connected"){
        game.state = GameState.NOT_SPAWNED;
        delayFunction(function(){
            println("Loged-in to the mainframe.");
            delayFunction(function(){
                println("Welkom " + game.name + ".");
            });
        });
    }
}

function notSpawnedCommand(command){
    switch(command[0]){
        case "spawn":
            game.ws.send("spawn#");
            println("Generating avatar.");
            break;
        default:
            println("Unkown command");
            break;
    }
}

function notSpawnedMessage(message){
    switch(message[0]){
        case "spawn":
            game.state = GameState.SPAWNED;
            println("Avatar generated, player spawned.");
            break
    }
}

function spawnedCommand(command){
    switch(command[0]){
        case "for":
        case "back":
        case "left":
        case "right":
        case "north":
        case "east":
        case "west":
        case "south":
        case "shoot":
        case "serround":
        case "map":
            game.ws.send(command[0] + "#");
            break;
        default:
            println("Unkown command");
            break;
    }
}

function spawnedMessage(message){
    switch(message[0]){
        case "serround":
            println("Serroundings:");
            for(var i = 1; i < message.length-1;i++){
                println(message[i].split('').join(' '));
            }
            break;
        case "obs_wall":
            println("There is a wall in the way.");
            break;
        case "obs_player":
            println("There is a player in the way.");
            break;
        case "shoot":
            if(message[1] === "killed"){
                println("You hit a player and killed him.");
            }else if(message[1] === "alive"){
                println("You hit a player.");
            }else if(message[1] === "miss"){
                println("You shoot and hit a wall");
            }
            break;
        case "hit":
            println("You are hit by a bullet!!");
            break;
        case "killed":
            println("You were killed.");
            game.state = GameState.NOT_SPAWNED;
            break;
        default:
            break;
    }
}

function handleCommand(command){
    println(">" + command);
    var com = command.replace('#','_').split(' ');
    if(game.state === GameState.LOGIN){
        loginCommand(com);
    }else if(!anyCommand(com)){
        switch(game.state){
            case GameState.NOT_SPAWNED:
                notSpawnedCommand(com);
            case GameState.SPAWNED:
                spawnedCommand(com);
            default:
                break;
        }
    }
}

function handleMessage(msg){
    msg = msg.data.split('#');
    if(!anyMessage(msg)){
        switch(game.state){
            case GameState.LOGIN:
                loginMessage(msg);
                break;
            case GameState.NOT_SPAWNED:
                notSpawnedMessage(msg);
                break;
            case GameState.SPAWNED:
                spawnedMessage(msg);
                break;
            default:
                break;
        }
    }
}

function start(){
    game = new Game($('#textbox'));
    $('html').click(function(){
        $('#input').focus();
    });
    $('textarea').val("");
    $('textarea').keyup(function(e){
        if(e.keyCode == 13){
            var text = $(this).val();
            handleCommand(text.replace('\n',''));
            $(this).val("");
        }
    });
}

function delayFunction(f){
    setTimeout(f,0);
}
