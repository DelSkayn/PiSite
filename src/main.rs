#![allow(dead_code)]

#[macro_use]
extern crate nickel;
#[macro_use]
extern crate log as log_lib;


mod game;
mod log;

use game::GameServer;

use log::SimpleLogger;


use std::str::FromStr;

use nickel::{
    Nickel,
    StaticFilesHandler,
    HttpRouter,
};

use std::collections::HashMap;

use std::sync::atomic::AtomicUsize;
use std::sync::atomic::Ordering;


fn main(){
    SimpleLogger::init().unwrap();

    let _game_server = GameServer::new();

    let mut server = Nickel::new();

    let req_count = AtomicUsize::new(0);

    server.utilize(StaticFilesHandler::new("res/lib/"));
    server.utilize(StaticFilesHandler::new("res/src/"));
    server.utilize(StaticFilesHandler::new("res/style/"));

    server.get("/",middleware!{|_,res|
        println!("/ Requested");
        let count = req_count.fetch_add(1,Ordering::AcqRel);
        let mut data = HashMap::new();
        data.insert("req_count",format!("{}",count));
        data.insert("/",String::from_str("true").unwrap());
        return res.render("res/index.mustage", &data);
    });

    server.get("/about",middleware!{|_,res|
        println!("about Requested");
        let mut data = HashMap::new();
        data.insert("about",true);
        return res.render("res/about.mustage", &data);
    });

    server.get("/game",middleware!{|_,res|
        println!("game Requested");
        let mut data = HashMap::new();
        if cfg!(debug_assertions) {
            data.insert("addr","\"ws://[::1]:8899\"");
        }else{
            data.insert("addr","\"ws://isuckatwebdev.duckdns.org:443\"");
        }
        return res.render("res/game.mustage", &data);
    });

    if cfg!(debug_assertions) {
        println!("Debug!");
        server.listen("localhost:3000");
    }else{
        server.listen("192.168.178.189:3000");
    }
}
