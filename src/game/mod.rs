extern crate ws;

extern crate rand;



use std::thread::{
    Builder as ThreadBuilder,
    JoinHandle,
};

mod data;

use self::data::MoveObstruction;
use self::data::Direction;
use self::data::Orientation;
use self::data::HitTarget;
use self::data::handle;

use std::rc::Rc;
use std::cell::RefCell;
use std::collections::HashMap;

use std::borrow::Borrow;
use std::borrow::BorrowMut;

use self::data::Game;
use std::str::FromStr;
use self::ws::*;

type ServerDataRef = Rc<ServerData>;

struct ServerData{
    data: Game,
    clients: RefCell<HashMap<u64,Sender>>,
}

impl ServerData{

    fn add_client(&self,id: u64,sender: Sender){
        self.clients.borrow_mut().insert(id,sender);
    }
    
    fn broadcast_other(&self,self_id: u64,message: &str) -> Result<()>{
        for (id,sender) in self.clients.borrow().iter(){
            if id != &self_id {
                println!("Send message: \"{}\" to {}",message,id);
                try!(sender.send(message));
            }
        }
        Ok(())
    }

    fn send_to(&self,to_id: u64,message: &str) -> Result<()>{
        match self.clients.borrow().get(&to_id){
            Some(x) => x.send(message),
            None => {
                println!("Tried to send to id: {} which does not exist",to_id);
                Ok(())
            },
        }
    }
}

struct Server{
    data: Rc<ServerData>,
}

impl Server{
    fn new() -> Self{
        Server{
            data: Rc::new(
            ServerData{
                data: Game::new(),
                clients: RefCell::new(HashMap::new()),
            }),
        }
    }
}

impl Factory for Server{
    type Handler = ClientHandler;

    fn connection_made(&mut self,out: Sender) -> Self::Handler{
        let r: &ServerData = self.data.borrow();
        let id = r.data.create_player();
        //send id to client
        out.send(format!("login-id#{}",id)).unwrap();
        self.data.add_client(id,out.clone());
        ClientHandler::new(self.data.clone(),out,id)
    }
}

struct ClientHandler{
    server: ServerDataRef,
    out: Sender,
    id: u64,
}

impl ClientHandler{
    fn new(server: ServerDataRef,out: Sender,id: u64) -> Self{
        ClientHandler{
            server: server,
            out: out,
            id: id,
        }
    }

    fn handle_message(&mut self,typ: &str,data: &str) -> Result<()>{
        match typ{
            "username" => {
                handle(self.server.data.set_name(self.id,data.to_string()),|_|{
                info!("User aquired username: {}",data);
                //let other clients know a new player connected.
                try!(self.server.broadcast_other(self.id,&format!("new_player#{}",data)));
                //Send connection confermation.
                self.out.send("connected#")
                })
            }
            "say" => {
                let name = match self.server.data.get_name(self.id){
                    Some(x) => x,
                    None => {
                        warn!("Player with no name tried to say something!");
                        return Ok(())
                    } 
                };
                self.server.broadcast_other(self.id,&format!("say#{}#{}",name,data))
            }
            "spawn" => {
                handle(self.server.data.spawn(self.id),|_|{
                    try!(self.out.send("spawn#"));
                        handle(self.server.data.get_surroundings(self.id),
                        |e|{
                            self.out.send(format!("serround#{}",e))
                    })
                })
            }
            "serround" => {
                    handle(self.server.data.get_surroundings(self.id),
                    |e|{
                        self.out.send(format!("serround#{}",e))
                })
            }
            "ping" => {
                self.out.send("pong#")
            },
            "for" => {
                handle(self.server.data.move_player(self.id,Direction::Forward),
                    |e|{
                        match e {
                            MoveObstruction::Nothing => Ok(()),
                            MoveObstruction::Wall => {
                                self.out.send("obs_wall#")
                            },
                            MoveObstruction::Player => {
                                self.out.send("obs_player#")
                            },
                        }
                    })
            },
            "back" => {
                handle(self.server.data.move_player(self.id,Direction::Backward),
                    |e|{
                        match e {
                            MoveObstruction::Nothing => Ok(()),
                            MoveObstruction::Wall => {
                                self.out.send("obs_wall#")
                            },
                            MoveObstruction::Player => {
                                self.out.send("obs_player#")
                            },
                        }
                    })
            },
            "left" => {
                handle(self.server.data.move_player(self.id,Direction::Left),
                    |e|{
                        match e {
                            MoveObstruction::Nothing => Ok(()),
                            MoveObstruction::Wall => {
                                self.out.send("obs_wall#")
                            },
                            MoveObstruction::Player => {
                                self.out.send("obs_player#")
                            },
                        }
                    })
           },
            "right" => {
                handle(self.server.data.move_player(self.id,Direction::Right),
                    |e|{
                        match e {
                            MoveObstruction::Nothing => Ok(()),
                            MoveObstruction::Wall => {
                                self.out.send("obs_wall#")
                            },
                            MoveObstruction::Player => {
                                self.out.send("obs_player#")
                            },
                        }
                    })
            },
            "map" => {
                self.out.send(format!("serround#{}",self.server.data.map()))
            }
            "north" => {
                handle(self.server.data.orient_player(self.id,Orientation::North),|_|{
                    Ok(())
                })
            }
            "east" => {
                handle(self.server.data.orient_player(self.id,Orientation::East),|_|{
                    Ok(())
                })
            }
            "west" => {
                handle(self.server.data.orient_player(self.id,Orientation::West),|_|{
                    Ok(())
                })
            }
            "south" => {
                handle(self.server.data.orient_player(self.id,Orientation::South),|_|{
                    Ok(())
                })
            }
            "shoot" => {
                handle(self.server.data.player_shoot(self.id),|e|{
                        match e{
                            (HitTarget::Player(x),true) => {
                                try!(self.server.send_to(x,"killed#"));
                                self.out.send("shoot#killed")
                            },
                            (HitTarget::Player(x),false) => {
                                try!(self.server.send_to(x,"hit#"));
                                self.out.send("shoot#alive")
                            },
                            (HitTarget::Wall,false) => self.out.send("shoot#miss"),
                            _ => {
                                warn!("Shoot returned unreachable result");
                                Ok(())
                            }
                        }
                    })
            }
            _ => {
                warn!("recieved incorrect message!");
                Ok(())
            },
        }
    }
}

impl Handler for ClientHandler{

    fn on_open(&mut self, _: Handshake) -> Result<()>{
        info!("Gained connection");
        Ok(())
    }

    fn on_message(&mut self,msg: Message) -> Result<()>{
        trace!("{:?}",msg);
        match msg{
            Message::Text(text) => {
                //Decode messages
                let index = match text.find("#"){
                    Some(x) => x,
                    None => {
                        warn!("Recieved invalid message");
                        return Ok(());
                    },
                };
                let (typ,data) = text.split_at(index);
                let d = &data[1..data.len()];
                //handle messages
                self.handle_message(typ,d)
            }
            //TODO: handle binary
            _ => {
                warn!("recieved binary message!");
                Ok(())
            },
        }
    }


    fn on_close(&mut self, code: CloseCode, error: &str){
        match self.server.data.unspawn(self.id) {
            Ok(_) => {},
            Err(_) => warn!("Some thing went wrong during unspawning"),
        }
        match self.server.clients.borrow_mut().remove(&self.id){
            Some(_x) => {},
            _ => println!("Client already removed!"),
        }
        match code{
            CloseCode::Normal => println!("The client has quit the connection."),
            CloseCode::Away => println!("The client has left the site."),
            _ => println!("client encountered an error: {}", error),
        };
    }
}

pub struct GameServer{
    handle: JoinHandle<()>,
}


impl GameServer{
    pub fn new() -> Self{
        let handle = ThreadBuilder::new()
            .name(String::from_str("Game Server Thread").unwrap()).spawn(||{
                let addr = if cfg!(debug_assertions) {
                    "[::1]:8899"
                }else{
                    "192.168.178.189:4001"
                };

                WebSocket::new(Server::new())
                    .unwrap()
                    .listen(addr)
                    .unwrap();

            }).unwrap();
        GameServer{
            handle: handle,
        }
    }
}
