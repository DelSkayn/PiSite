use super::StdRng;

use super::pos::Position;
use super::pos::Orientation;

pub enum MoveObstruction{
    Wall,
    Player,
    Nothing,
}

#[derive(PartialEq,Eq)]
pub enum HitTarget{
    Wall,
    Player(u64),
    Nothing,
}

pub enum PlayerHit{
    Head,
    Torso,
    Arms,

}

#[derive(Clone,PartialEq,Eq)]
pub enum MapTile{
    Nothing,
    Wall,
    Player(u64),
}

#[derive(Clone)]
pub enum Direction{
    Forward,
    Backward,
    Left,
    Right,
}

impl Direction{
    pub fn into_offset(&self) -> (i32,i32){
        match self{
            &Direction::Forward => (0,-1),
            &Direction::Backward => (0,1),
            &Direction::Left => (-1,0),
            &Direction::Right => (1,0),
        }
    }
}


pub struct Map{
    map: Vec<Vec<MapTile>>,
}

impl Map{
    pub fn new(_rng: &StdRng, w: u32, h:u32) -> Self{
        let mut data = Vec::with_capacity(w as usize);
        for i in 0..w{
            data.push(Vec::with_capacity(h as usize));
            for _ in 0..h{
                data[i as usize].push(MapTile::Nothing);
            }
        }
        //todo add walls
        Map{
            map:data,
        }
    }

    pub fn width(&self) -> usize{
        self.map.len()
    }

    pub fn height(&self) -> usize{
        self.map[0].len()
    }

    pub fn print(&self){
        for x in 0..self.map.len(){
            for y in 0..self.map[x].len(){
                match self.map[x][y]{
                    MapTile::Nothing => print!("."),
                    MapTile::Wall => print!("x"),
                    MapTile::Player(_) => print!("@"),
                }
            }
            println!("");
        }
    }
    
    pub fn get_pos(&self,pos: &Position) -> MapTile{
        if self.inside(pos.x,pos.y) {
            return self.map[pos.x as usize][pos.y as usize].clone();
        }
        MapTile::Wall
    }

    pub fn get(&self,x: i32, y: i32) -> MapTile{
        if self.inside(x,y) {
            return self.map[x as usize][y as usize].clone();
        }
        MapTile::Wall
    }

    pub fn set_pos(&mut self,pos: &Position,value: MapTile){
        if self.inside(pos.x,pos.y) {
            self.map[pos.x as usize][pos.y as usize] = value;
        }
    }

    fn inside(&self,x:i32,y:i32) -> bool{
        Self::inside_static(self.map.len() as u32
                            ,self.map[0].len() as u32
                            ,x,y)
    }

    fn inside_static(w: u32,h: u32,x: i32, y: i32) -> bool{
        x >= 0 && x < (w as i32) && y >= 0 && y < (h as i32)
    }

    pub fn shoot(&self,pos: &Position,ori: Orientation) -> HitTarget{
        let mut range = 1;
        match ori {
            Orientation::North => {
                loop{
                    match self.get_pos(&(pos.clone() + Position::from_coords(0,-range))){
                        MapTile::Player(x) => {
                            return HitTarget::Player(x);
                        },
                        MapTile::Wall => {
                            return HitTarget::Wall;
                        },
                        _ => {},
                    }
                    range+=1;
                }
            }
            Orientation::South => {
                loop{
                    match self.get_pos(&(pos.clone() + Position::from_coords(0,range))){
                        MapTile::Player(x) => {
                            return HitTarget::Player(x);
                        },
                        MapTile::Wall => {
                            return HitTarget::Wall;
                        },
                        _ => {},
                    }
                    range+=1;
                }
            }
            Orientation::East => {
                loop{
                    match self.get_pos(&(pos.clone() + Position::from_coords(range,0))){
                        MapTile::Player(x) => {
                            return HitTarget::Player(x);
                        },
                        MapTile::Wall => {
                            return HitTarget::Wall;
                        },
                        _ => {},
                    }
                    range+=1;
                }
            }
            Orientation::West => {
                loop{
                    match self.get_pos(&(pos.clone() + Position::from_coords(-range,0))){
                        MapTile::Player(x) => {
                            return HitTarget::Player(x);
                        },
                        MapTile::Wall => {
                            return HitTarget::Wall;
                        },
                        _ => {},
                    }
                    range+=1;
                }
            }
        }
    }
}
