
use super::pos::Position;
use super::pos::Orientation;
use super::GameError;

use super::Result;

static HEAD_DAMAGE_MULTI: i32 = 4;
static LEGS_DAMAGE_MULTI: i32 = 1;
static ARMS_DAMAGE_MULTI: i32 = 1;
static TORSO_DAMAGE_MULTI: i32 = 2;
pub enum DamageTarget{
    Head,
    Arms,
    Legs,
    Torso,
}

///
///As structure representing the healt of the player while spawned
///
pub struct Health{
    pub head: i32,
    pub arms: i32,
    pub legs: i32,
    pub torso: i32,
}

impl Health{
    pub fn new() -> Self{
        Health{
            head: 12,
            arms: 12,
            legs: 12,
            torso: 12,
        }
    }
}

pub struct SpawnedInfo{
    pub pos: Position,
    pub ori: Orientation,
    health: Health,
}

impl SpawnedInfo{
    pub fn new(pos: Position) -> Self{
        SpawnedInfo{
            pos: pos,
            ori: Orientation::North,
            health: Health::new(),
        }
    }
}

pub struct Player{
    id: u64,
    name: String,
    spawned_info: Option<SpawnedInfo>, 
}

impl Player{
    pub fn new(id: u64,name:String) -> Self{
        Player{
            id: id,
            name: name,
            spawned_info: None,
        }
    }

    ///
    ///Returns wether to player is spawned
    ///
    pub fn is_spawned(&self) -> bool{
        self.spawned_info.is_some()
    }

    ///
    ///Spawn the player at pos
    ///
    pub fn spawn(&mut self,pos: Position){
        self.spawned_info = Some(
            SpawnedInfo::new(pos)
            );
    }

    ///
    /// Returns a reference to the spawned info of the player
    ///
    /// Panics: when the player isnt spawned
    ///
    pub fn get_spawn_info(&self) -> Result<&SpawnedInfo>{
        match self.spawned_info.as_ref(){
            Some(x) => Ok(x),
            None => Err(GameError::PlayerNotSpawned),
        }
    }

    ///
    /// Returns a mutable reference to the spawned info of the player
    ///
    /// Panics: when the player isnt spawned
    ///
    pub fn get_spawn_info_mut(&mut self) -> Result<&mut SpawnedInfo>{
        match self.spawned_info.as_mut(){
            Some(x) => Ok(x),
            None => Err(GameError::PlayerNotSpawned),
        }
    }

    ///
    ///Hit the player at target hittarget
    ///Returns whether this kills the player
    ///
    pub fn hit(&mut self,damage: i32,target: DamageTarget) -> Result<bool>{
        let info = try!(self.get_spawn_info_mut());
        match target {
            DamageTarget::Head => {
                info.health.head -= damage * HEAD_DAMAGE_MULTI;
                Ok(info.health.head <= 0)
            },
            DamageTarget::Torso => {
                info.health.torso -= damage * TORSO_DAMAGE_MULTI;
                Ok(info.health.torso <= 0)
            },
            DamageTarget::Legs => {
                info.health.legs -= damage * LEGS_DAMAGE_MULTI;
                Ok(info.health.legs <= 0)
            },
            DamageTarget::Arms => {
                info.health.arms -= damage * ARMS_DAMAGE_MULTI;
                Ok(info.health.arms <= 0)
            },
        }
    }

    pub fn hit_rand(&mut self,damage: i32,rand: i32) -> Result<bool>{
        let hit = match rand {
            0...10 => DamageTarget::Head,
            10...70 => DamageTarget::Torso,
            70...90 => DamageTarget::Arms,
            90...100 => DamageTarget::Legs,
            _ => {
                return Ok(false);
            },
        };
        self.hit(damage,hit)
    }

    pub fn move_to(&mut self,pos: Position) -> Result<()>{
        try!(self.get_spawn_info_mut()).pos = pos;
        Ok(())
    }

    pub fn get_name(&self) -> String{
        self.name.clone()
    }

    pub fn set_name(&mut self,name: String){
        self.name = name;
    }
}
