
use std::collections::HashMap;

use std::cell::RefCell;
pub use super::rand::StdRng;
pub use super::rand::Rng;

mod map;
mod pos;
mod player;

use self::map::Map;
pub use self::map::MapTile;
pub use self::map::HitTarget;
pub use self::map::Direction;
pub use self::map::MoveObstruction;
pub use self::pos::Position;
pub use self::pos::Orientation;
pub use self::player::Player;

pub struct Game{
    data: RefCell<GameData>,
}

struct GameData{
    next_id: u64,
    players: HashMap<u64,Player>,
    map: Map,
    rng: StdRng,
}

enum GameError{
    PlayerDoesntExist,
    PlayerNotSpawned,
}

use std::result::Result as StdResult;
pub type Result<T> = StdResult<T,GameError>;

pub fn handle<T,F,R>(res: Result<T>,func: F) -> StdResult<(),R>
where F: FnOnce(T) -> StdResult<(),R>{
    match res{
        Ok(x) => func(x),
        Err(x) => {
            match x{
                GameError::PlayerDoesntExist => warn!("Tried to access player which does not exist"),
                GameError::PlayerNotSpawned=> warn!("Tried to access spawn data of player which is not spawned"),
            };
            StdResult::Ok(())
        }
    }
}

impl Game{
    pub fn new() -> Self{
        Game{
            data: RefCell::new(GameData::new()),
        }
    }

    pub fn create_player(&self) -> u64{
        let mut borrow = self.data.borrow_mut();
        borrow.create_player()
    }

    pub fn set_name(&self,id: u64, name: String) -> Result<()>{
        let mut borrow = self.data.borrow_mut();
        borrow.set_name(id,name)
    }

    pub fn get_name<'a>(&'a self,id: u64) -> Option<String>{
        self.data.borrow().players.get(&id).map(|x| x.get_name())
    }

    pub fn generate_map(&self,width: u32,height: u32){
        let borrow = &mut self.data.borrow_mut();
        borrow.generate_map(width,height)
    }

    pub fn spawn(&self,id: u64) -> Result<()>{
        let borrow = &mut self.data.borrow_mut();
        borrow.spawn(id)
    }

    pub fn unspawn(&self,id: u64) -> Result<()>{
        let borrow = &mut self.data.borrow_mut();
        borrow.unspawn(id)
    }

    pub fn move_player(&self,id: u64,dir: Direction) -> Result<MoveObstruction>{
        let borrow = &mut self.data.borrow_mut();
        borrow.move_player(id,dir)
    }

    pub fn orient_player(&self,id: u64,or: Orientation) -> Result<()>{
        let borrow = &mut self.data.borrow_mut();
        borrow.orient_player(id,or)
    }

    pub fn get_surroundings(&self,id: u64) -> Result<String>{
        let borrow = &mut self.data.borrow_mut();
        borrow.get_surroundings(id)
    }

    pub fn map(&self) -> String{
        let borrow = &mut self.data.borrow_mut();
        borrow.map()
    }

    pub fn player_shoot(&self,id: u64) -> Result<(HitTarget,bool)>{
        let borrow = &mut self.data.borrow_mut();
        borrow.player_shoot(id)
    }
}

impl GameData{
    fn new() -> Self{
        let rng = StdRng::new().unwrap();
        GameData{
            next_id: 0,
            players: HashMap::new(),
            map: Map::new(&rng,8,8),
            rng: rng, 
        }
    }

    fn get_player(&mut self,id: u64) -> Result<&Player>{
        match self.players.get(&id){
            Some(x) => Ok(x),
            None => Err(GameError::PlayerDoesntExist),
        }
    }

    fn get_player_mut(&mut self,id: u64) -> Result<&mut Player>{
        match self.players.get_mut(&id){
            Some(x) => Ok(x),
            None => Err(GameError::PlayerDoesntExist),
        }
    }

    pub fn create_player(&mut self) -> u64{
        let id = self.next_id;
        let player = Player::new(id,"".to_string());
        self.players.insert(id,player);
        self.next_id += 1;
        id
    }

    pub fn set_name(&mut self,id: u64, name: String) -> Result<()>{
        try!(self.get_player_mut(id)).set_name(name);
        Ok(())
    }

    pub fn generate_map(&mut self,width: u32,height: u32){
        self.map = Map::new(&self.rng,width,height);
        self.map.print();
    }

    pub fn spawn(&mut self,id: u64) -> Result<()>{
        let w = self.map.width();
        let h = self.map.height();
        //TODO when walls are added test your not in the wall.
        let mut pos;
        while{
            pos = Position::from_rand(&mut self.rng,w as i32,h as i32);
            self.map.get_pos(&pos) != MapTile::Nothing
        }{};

        try!(self.get_player_mut(id)).spawn(pos.clone());
        self.map.set_pos(&pos,MapTile::Player(id));
        Ok(())
    }

    pub fn unspawn(&mut self,id: u64) -> Result<()>{
        let pos;
        {
            let player = try!(self.get_player_mut(id));
            pos = try!(player.get_spawn_info_mut()).pos.clone();
        }
        self.players.remove(&id);
        self.map.set_pos(&pos,MapTile::Nothing);
        Ok(())
    }

    pub fn move_player(&mut self,id: u64,dir: Direction) -> Result<MoveObstruction>{
        let (offset_x,offset_y) = dir.into_offset();
        let previous;
        let new;
        {
            //get the new and previous coords from the player
            let x = try!(self.get_player(id));
            previous = try!(x.get_spawn_info()).pos.clone();
            new = previous.clone() 
                + Position::from_coords(offset_x,offset_y);

        }
        //test if there is nothing in the way
        match self.map.get_pos(&new) {
            MapTile::Wall => return Ok(MoveObstruction::Wall),
            MapTile::Player(_) => return Ok(MoveObstruction::Player),
            MapTile::Nothing => {},
        };
        try!(try!(self.get_player_mut(id)).move_to(new.clone()));
        self.map.set_pos(&previous,MapTile::Nothing);
        self.map.set_pos(&new,MapTile::Player(id));
        info!("New player pos: {},{}",new.x,new.y);

        //do the actual moving
        Ok(MoveObstruction::Nothing)
    }

    pub fn orient_player(&mut self,id: u64,ori: Orientation) -> Result<()>{
        try!(try!(self.get_player_mut(id)).get_spawn_info_mut()).ori = ori;
        Ok(())
    }

    pub fn get_surroundings(&mut self,id: u64) -> Result<String>{
        let pos = try!(try!(self.get_player(id)).get_spawn_info()).pos.clone();
        let mut s = String::new();
        for i in -1..2{
            for j in -1..2{
                s.push(match self.map.get(j+pos.x,i+pos.y){
                    MapTile::Nothing => '.',
                    MapTile::Wall => 'x',
                    MapTile::Player(_) => '@',
                });
            }
            s.push('#');
        }
        return Ok(s); 
    }

    pub fn map(&mut self) -> String{
        let mut s = String::new();
        for i in -1..(self.map.width()+1) as i32{
            for j in -1..(self.map.height()+1) as i32{
                s.push(match self.map.get(j,i){
                    MapTile::Nothing => '.',
                    MapTile::Wall => 'x',
                    MapTile::Player(_) => '@',
                });
            }
            s.push('#');
        }
        return s; 
    }

    ///
    /// returns what it hit and if it killed it
    ///
    pub fn player_shoot(&mut self,id: u64) -> Result<(HitTarget,bool)>{
        let pos;
        let ori;
        {
            let player = try!(self.get_player_mut(id));
            let info = try!(player.get_spawn_info());
            pos = info.pos.clone();
            ori = info.ori.clone();
        }
        let hit = self.map.shoot(&pos,ori.clone());
        if let HitTarget::Player(x) = hit {
            let rand = self.rng.gen_range(0,100);
            let is_dead = try!(try!(self.get_player_mut(id)).hit_rand(3,rand));
            if is_dead {
                try!(self.unspawn(x));
            }
            Ok((hit,is_dead))
        }else{
            Ok((hit,false))
        }
    }
}
