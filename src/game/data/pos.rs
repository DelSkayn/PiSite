use super::StdRng;
use super::Rng;
use std::ops::{Add,Sub};


#[derive(Clone)]
pub struct Position{
    pub x: i32,
    pub y: i32,
}

impl Position{
    pub fn new() -> Self{
        Position{
            x: 0,
            y: 0,
        }
    }

    pub fn from_coords(x: i32,y: i32) -> Self{
        Position{
            x: x,
            y: y,
        }
    }

    pub fn from_rand(rng: &mut StdRng,w: i32,h:i32) -> Self{
        Position{
            x: rng.gen_range(0,w),
            y: rng.gen_range(0,h),
        }
    }

    pub fn distance(&self,other: &Position) -> i32{
        (self.x - other.x).abs() + (self.y - other.y).abs()
    }
}

impl Add<Position> for Position{
    type Output = Position;

    fn add(mut self,other:Position) -> Position{
        self.x += other.x;
        self.y += other.y;
        self
    }
}

impl Sub<Position> for Position{
    type Output = Position;

    fn sub(mut self,other:Position) -> Position{
        self.x += other.x;
        self.y += other.y;
        self
    }
}

#[derive(Clone)]
pub enum Orientation{
    North,//+y
    East,//+x
    South,//-y
    West,//-x
}
